const fs = require('fs')




const requestHandle = (  req, res ) => {
    let { url, method } = req

    if (url === '/') {
        res.setHeader('content-type', 'Text/html')
        // write crearte page on browser
        res.write('<html><head><title>Enter you messsage</title><body><form action="/message" method="POST"><input type="text" name="message"/><button>send</button></form></body></head><html/>')
        //end will stop the process 
        return res.end()
    }


    if (url === '/message' && method === "POST") {
        //staream the sqence of the data move one point to another point on over the time 
        //on read event || data event fire when new chunck ready to be read
        const body = [];
        //we edit the data behind it's self not the value it's self we change the body object 
        req.on("data", (chunk) => {
            body.push(chunk)
            console.log('chunk: ', chunk);
        })

        return req.on('end', () => {
            const parseData = Buffer.concat(body).toString()
            console.log('parseData: ', parseData);  
            //create file in local and add message we have two types of writefile methods sync and async 
            fs.writeFile('message.txt', parseData, (err) => {
                res.statusCode = 302;
                res.setHeader("Location", '/')
                return res.end()
            })
        })
    }

    res.setHeader('content-type', 'Text/html')
    res.write('<html><head><title>first node js page</title><body><h1>this is the first html page create by node js </h1></body></head><html/>')
    res.end()

    //exit terminate the serever
    // process.exit()
}


module.exports = requestHandle